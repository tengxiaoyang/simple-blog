import Vue from 'vue'
import App from './App.vue'

import router from './router' // 引入 router文件夹下的 index.js
import './plugins/element.js' // 引入 element-ui

Vue.config.productionTip = false

import axios from 'axios' // 引入 axios
import store from './store'
Vue.prototype.$http = axios.create({ // 将 axios方法定义到vue的原型上
  baseURL: 'http://127.0.0.1:3000/api' // 基础url，前端发起请求要先拼接上这个地址
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
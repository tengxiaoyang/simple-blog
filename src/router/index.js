import Vue from 'vue'
import VueRouter from 'vue-router'
// 分别引入三个 view
import ListArticle from '../views/ListArticle.vue' 
import CreateArticle from '../views/CreateArticle.vue'
import EditArticle from '../views/EditArticle.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    redirect: '/article/index' // 访问根路径，重定向到 /article/index
  },
  {
    path: '/article/index',
    name: 'List-article',
    component: ListArticle
  },
  {
    path: '/article/create',
    name: 'create-article',
    component: CreateArticle
  },
  {
    path: '/article/:id/edit',
    name: 'edit-article',
    component: EditArticle
  }
]

const router = new VueRouter({
  routes
})

export default router